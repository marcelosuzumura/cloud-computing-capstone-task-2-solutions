package cloudcomputingcapstone.kafka;

import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class DataProducer {

	public static void main(String[] args) throws Exception {
		if (args.length < 3) {
			System.err.println(
					"wrong usage! use: mvn clean package -Dmaven.test.skip && java -jar target/kafka-producer-1.0.0-SNAPSHOT.jar \"localhost:9092\" \"/home/marcelo/workspace/clean-2008\" \"test-data-2\"");
			System.exit(1);
		}

		// "localhost:9092" "/home/marcelo/workspace/clean-2008" "test-data-1"
		String servers = args[0];
		String dataDirectory = args[1];
		String topic = args[2];

		Properties props = new Properties();
		props.put("bootstrap.servers", servers);
		props.put("acks", "all");
		props.put("retries", 0);
		props.put("batch.size", 16384);
		props.put("linger.ms", 1);
		props.put("buffer.memory", 33554432);
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

		Producer<String, String> producer = new KafkaProducer<String, String>(props);

		Collection<File> files = FileUtils.listFiles(new File(dataDirectory), new String[] { "csv" }, false);
		long totalLines = 0;
		for (File file : files) {
			System.out.println(file.getName());
			List<String> lines = IOUtils.readLines(new FileReader(file));

			for (int i = 0; i < lines.size(); i++) {
				String line = lines.get(i);
				producer.send(new ProducerRecord<String, String>(topic, Integer.toString(i), line));
				totalLines++;

				if (i % 100000 == 0) {
					System.out.println(i + " " + line);
					System.out.println("total lines from all files until now: " + totalLines);
				}
			}
		}
		System.out.println("total lines from all files: " + totalLines);

		producer.close();
	}

}
