package cloudcomputingcapstone.task2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaPairReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;

import cloudcomputingcapstone.Constants;
import cloudcomputingcapstone.Utils;
import cloudcomputingcapstone.cassandra.CassandraDriver;
import cloudcomputingcapstone.cassandra.ColumnDefinition;
import kafka.serializer.StringDecoder;
import scala.Tuple2;

public final class Q32 {

	public static void main(String[] args) {
		if (args.length < 5) {
			System.err.println("Usage: <zkQuorum> <group> <topics> <numThreads> <cassandra>");
			System.exit(1);
		}

		String zkQuorum = args[0];
		String consumerGroup = args[1];
		String[] topics = args[2].split(",");
		int numThreads = Integer.parseInt(args[3]);
		final String cassandraIP = args[4];

		SparkConf sparkConf = new SparkConf().setAppName(Q32.class.getName());
		sparkConf.set("spark.streaming.unpersist", "false");
		sparkConf.set("spark.streaming.receiver.writeAheadLog.enable", "true");

		JavaStreamingContext jssc = new JavaStreamingContext(sparkConf, Durations.seconds(3));
		jssc.checkpoint("/tmp");

		final Accumulator<Integer> flightsProcessed = jssc.sparkContext().intAccumulator(0);
		final Accumulator<Double> start = jssc.sparkContext().accumulator(System.currentTimeMillis());
		final Accumulator<Double> dbReading = jssc.sparkContext().accumulator(0f);
		final Accumulator<Double> dbInserting = jssc.sparkContext().accumulator(0f);

		Map<String, Integer> topicMap = new HashMap<String, Integer>();
		for (String topic : topics) {
			topicMap.put(topic, 1);
		}

		HashMap<String, String> kafkaParams = new HashMap<String, String>();
		kafkaParams.put("zookeeper.connect", zkQuorum);
		kafkaParams.put("group.id", consumerGroup);
		kafkaParams.put("zookeeper.connection.timeout.ms", "10000");
		kafkaParams.put("auto.offset.reset", "smallest");

		// parallel reading from Kafka
		List<JavaPairDStream<String, String>> kafkaStreams = new ArrayList<JavaPairDStream<String, String>>(numThreads);
		for (int i = 0; i < numThreads; i++) {
			JavaPairReceiverInputDStream<String, String> messages = KafkaUtils.createStream(jssc, String.class,
					String.class, StringDecoder.class, StringDecoder.class, kafkaParams, topicMap,
					StorageLevel.MEMORY_AND_DISK_SER());
			kafkaStreams.add(messages);
		}
		JavaPairDStream<String, String> unifiedStream = jssc.union(kafkaStreams.get(0),
				kafkaStreams.subList(1, kafkaStreams.size()));

		JavaDStream<String> lines = Utils.getLines(unifiedStream, flightsProcessed);

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		JavaPairDStream<String, FlightCSVFormat> allFlightsStream = lines
				.mapToPair(new PairFunction<String, String, FlightCSVFormat>() {

					private static final long serialVersionUID = -7986497144086431718L;

					@Override
					public Tuple2<String, FlightCSVFormat> call(String x) {
						FlightCSVFormat flightCSVFormat = FlightCSVFormat.parseCSVLine(x);

						return new Tuple2<String, FlightCSVFormat>(
								flightCSVFormat.getFlightKeyForMininumDelayMapReduce(), flightCSVFormat);
					}
				});

		JavaPairDStream<String, FlightCSVFormat> firstLegFlights = allFlightsStream
				.filter(new Function<Tuple2<String, FlightCSVFormat>, Boolean>() {

					private static final long serialVersionUID = 5561288034025651175L;

					@Override
					public Boolean call(Tuple2<String, FlightCSVFormat> v1) throws Exception {
						if (v1._1().endsWith("1")) {
							return true;
						}
						return false;
					}

				});

		JavaPairDStream<String, FlightCSVFormat> secondLegFlights = allFlightsStream
				.filter(new Function<Tuple2<String, FlightCSVFormat>, Boolean>() {

					private static final long serialVersionUID = 5561288034025651175L;

					@Override
					public Boolean call(Tuple2<String, FlightCSVFormat> v1) throws Exception {
						if (v1._1().endsWith("2")) {
							return true;
						}
						return false;
					}

				});

		final List<ColumnDefinition> columns = new ArrayList<>();
		columns.add(new ColumnDefinition("x", "text"));
		columns.add(new ColumnDefinition("y", "text"));
		columns.add(new ColumnDefinition("day", "int"));
		columns.add(new ColumnDefinition("month", "int"));
		columns.add(new ColumnDefinition("year", "int"));
		columns.add(new ColumnDefinition("flight_xy", "text"));
		columns.add(new ColumnDefinition("dep_time", "text"));
		columns.add(new ColumnDefinition("delay", "int"));
		final String[] primaryKeyColumns = new String[] { "x", "y", "day", "month", "year" };

		firstLegFlights.foreachRDD(new VoidFunction<JavaPairRDD<String, FlightCSVFormat>>() {

			private static final long serialVersionUID = 7234764440325615097L;

			@Override
			public void call(JavaPairRDD<String, FlightCSVFormat> rdd) {
				String keyspace = "t2g32";
				String table = keyspace + "." + keyspace;
				String tableSuffix = "leg1";
				String fullTableName = table + tableSuffix;
				CassandraDriver cassandra = new CassandraDriver(cassandraIP, keyspace, tableSuffix, columns,
						primaryKeyColumns);
				Session cassandraSession = cassandra.getSession();

				////////////////////

				updateMinimumDelayFlights(rdd, fullTableName, cassandraSession, dbReading, dbInserting);

				cassandra.close();

				Utils.printProgress(Constants.TOTAL_FLIGHTS_32, flightsProcessed, start);
				System.out.println("read db: " + (dbReading.value() / 1000) + "s, insert db: "
						+ (dbInserting.value() / 1000) + "s");
			}
		});

		secondLegFlights.foreachRDD(new VoidFunction<JavaPairRDD<String, FlightCSVFormat>>() {

			private static final long serialVersionUID = 7234764440325615097L;

			@Override
			public void call(JavaPairRDD<String, FlightCSVFormat> rdd) {
				String keyspace = "t2g32";
				String table = keyspace + "." + keyspace;
				String tableSuffix = "leg2";
				String fullTableName = table + tableSuffix;
				CassandraDriver cassandra = new CassandraDriver(cassandraIP, keyspace, tableSuffix, columns,
						primaryKeyColumns);
				Session cassandraSession = cassandra.getSession();

				////////////////////

				updateMinimumDelayFlights(rdd, fullTableName, cassandraSession, dbReading, dbInserting);

				cassandra.close();

				Utils.printProgress(Constants.TOTAL_FLIGHTS_32, flightsProcessed, start);
				System.out.println("read db: " + (dbReading.value() / 1000) + "s, insert db: "
						+ (dbInserting.value() / 1000) + "s");
			}
		});

		// Start the computation
		jssc.start();
		jssc.awaitTermination();
	}

	private static void updateMinimumDelayFlights(JavaPairRDD<String, FlightCSVFormat> rdd, String fullTableName,
			Session cassandraSession, Accumulator<Double> dbReading, Accumulator<Double> dbInserting) {
		for (Tuple2<String, FlightCSVFormat> t : rdd.collect()) {
			FlightCSVFormat flightCSVFormat = t._2();
			String origin = flightCSVFormat.getOrigin();
			String destination = flightCSVFormat.getDestination();
			int day = Integer.valueOf(flightCSVFormat.getDay());
			int month = Integer.valueOf(flightCSVFormat.getMonth());
			int year = Integer.valueOf(flightCSVFormat.getYear());
			String flight = flightCSVFormat.getFlightNum();
			String departure = flightCSVFormat.getDepartureTime();
			int delay = flightCSVFormat.getArrivalDelay();

			// search for current delay
			long startReading = System.currentTimeMillis();
			ResultSet resultSet = cassandraSession
					.execute("SELECT delay FROM " + fullTableName + " WHERE x = '" + origin + "' AND y = '"
							+ destination + "' AND day = " + day + " AND month = " + month + " AND year = " + year);
			int currentDelay = Integer.MAX_VALUE;
			if (!resultSet.isExhausted()) {
				currentDelay = resultSet.one().getInt("delay");
			}
			dbReading.add(Double.valueOf(System.currentTimeMillis() - startReading));

			if (delay < currentDelay) { // strictly lower
				// "upsert"
				long startInserting = System.currentTimeMillis();
				cassandraSession.execute("INSERT INTO " + fullTableName
						+ " (x, y, day, month, year, flight_xy, dep_time, delay) VALUES ('" + origin + "', '"
						+ destination + "', " + day + ", " + month + ", " + year + ", '" + flight + "', '" + departure
						+ "', " + delay + ")");
				dbInserting.add(Double.valueOf(System.currentTimeMillis() - startInserting));
			}
		}
	}

}
