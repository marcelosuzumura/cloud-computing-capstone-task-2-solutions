package cloudcomputingcapstone.task2;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

class FlightCSVFormat implements Serializable {

	private static final long serialVersionUID = 3773400940604959131L;

	private String year;
	private String month;
	private String day;
	private String flightNum;
	private String origin;
	private String destination;
	private String departureTime;
	private int arrivalDelay;

	public FlightCSVFormat() {
		//
	}

	private FlightCSVFormat(String year, String month, String day, String flightNum, String origin, String destination,
			String departureTime, int arrivalDelay) {
		this();
		this.year = year;
		this.month = month;
		this.day = day;
		this.flightNum = flightNum;
		this.origin = origin;
		this.destination = destination;
		this.departureTime = departureTime;
		this.arrivalDelay = arrivalDelay;
	}

	public static FlightCSVFormat parseCSVLine(String value) {
		String[] values = value.split(",");

		try {
			String year = values[0];
			String month = values[1];
			String day = values[2];
			String flightNum = values[3];
			String origin = values[5];
			String destination = values[6];
			String departureTime = values[7];
			int arrivalDelay = Integer.valueOf(values[9]);

			return new FlightCSVFormat(year, month, day, flightNum, origin, destination, departureTime, arrivalDelay);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("array out of bounds exception: " + value.toString());
			throw e;
		}
	}

	public int getDepartureTimeHour() {
		// get the hour from time: e.g. 903 / 100 = 9; 2050 / 100 = 20
		int departureHour = Integer.valueOf(this.departureTime) / 100;
		return departureHour;
	}

	public int getDepartureTimeMinutes() {
		// get the minutes from time: e.g. 903 % 100 = 3; 2050 % 100 = 50
		int departureMinutes = Integer.valueOf(this.departureTime) % 100;
		return departureMinutes;
	}

	public String getFlightKeyForMininumDelayMapReduce() {
		String key = this.origin + "," + this.destination + "," + this.day + "," + this.month + "," + this.year;
		if (this.canBeFirstLegFlight()) {
			return key + ",1";
		} else {
			return key + ",2";
		}
	}

	public boolean canBeFirstLegFlight() {
		boolean canBeFirstLegFlight;
		if (this.getDepartureTimeHour() < 12
				|| (this.getDepartureTimeHour() == 12 && this.getDepartureTimeMinutes() == 0)) {
			canBeFirstLegFlight = true;
		} else {
			canBeFirstLegFlight = false;
		}
		return canBeFirstLegFlight;
	}

	public String getYear() {
		return this.year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return this.month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getDay() {
		return this.day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getFlightNum() {
		return this.flightNum;
	}

	public void setFlightNum(String flightNum) {
		this.flightNum = flightNum;
	}

	public String getOrigin() {
		return this.origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return this.destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDepartureTime() {
		return this.departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public int getArrivalDelay() {
		return this.arrivalDelay;
	}

	public void setArrivalDelay(int arrivalDelay) {
		this.arrivalDelay = arrivalDelay;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SIMPLE_STYLE);
	}

}
