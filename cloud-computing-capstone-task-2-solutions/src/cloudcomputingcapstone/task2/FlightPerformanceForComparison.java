package cloudcomputingcapstone.task2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class FlightPerformanceForComparison implements Comparable<FlightPerformanceForComparison>, Serializable {

	private static final long serialVersionUID = -6252178291989929446L;

	public String criteria1;
	public String criteria2;
	public float performance;

	@Override
	public int compareTo(FlightPerformanceForComparison flight) {
		// order desc
		if (this.performance < flight.performance) {
			return 1;
		} else if (this.performance > flight.performance) {
			return -1;
		}
		return 0;
	}

	public static void main(String[] args) {
		FlightPerformanceForComparison f1 = new FlightPerformanceForComparison();
		f1.criteria1 = "a";
		f1.performance = 0.8f;

		FlightPerformanceForComparison f2 = new FlightPerformanceForComparison();
		f2.criteria1 = "b";
		f2.performance = 0.7f;

		FlightPerformanceForComparison f3 = new FlightPerformanceForComparison();
		f3.criteria1 = "c";
		f3.performance = 1.0f;

		FlightPerformanceForComparison f4 = new FlightPerformanceForComparison();
		f4.criteria1 = "d";
		f4.performance = 1.0f;

		List<FlightPerformanceForComparison> list = new ArrayList<>();
		list.add(f1);
		list.add(f2);
		list.add(f3);
		list.add(f4);

		Collections.sort(list);

		for (FlightPerformanceForComparison flightPerformanceForComparison : list) {
			System.out.println(
					flightPerformanceForComparison.performance + ", " + flightPerformanceForComparison.criteria1);
		}
	}

}
