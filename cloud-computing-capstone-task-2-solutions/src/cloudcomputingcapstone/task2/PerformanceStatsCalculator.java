package cloudcomputingcapstone.task2;

import java.io.Serializable;

class PerformanceStatsCalculator implements Serializable {

	private static final long serialVersionUID = -4173259922933369635L;

	public int stat;
	public int total;

	public float get() {
		return (float) this.stat / this.total;
	}

}
