package cloudcomputingcapstone.task2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaPairReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.google.common.base.Optional;

import cloudcomputingcapstone.Utils;
import cloudcomputingcapstone.cassandra.CassandraDriver;
import cloudcomputingcapstone.cassandra.ColumnDefinition;
import kafka.serializer.StringDecoder;
import scala.Tuple2;

public final class Q12 {

	private static final Pattern COMMA = Pattern.compile(",");

	public static void main(String[] args) {
		if (args.length < 5) {
			System.err.println("Usage: <zkQuorum> <group> <topics> <numThreads> <cassandra>");
			System.exit(1);
		}

		String zkQuorum = args[0];
		String consumerGroup = args[1];
		String[] topics = args[2].split(",");
		int numThreads = Integer.parseInt(args[3]);
		final String cassandraIP = args[4];

		SparkConf sparkConf = new SparkConf().setAppName(Q12.class.getName());
		sparkConf.set("spark.streaming.unpersist", "false");
		sparkConf.set("spark.streaming.receiver.writeAheadLog.enable", "true");

		JavaStreamingContext jssc = new JavaStreamingContext(sparkConf, Durations.seconds(5));
		jssc.checkpoint("/tmp");

		final Accumulator<Integer> flightsProcessed = jssc.sparkContext().intAccumulator(0);
		final Accumulator<Double> start = jssc.sparkContext().accumulator(System.currentTimeMillis());

		Map<String, Integer> topicMap = new HashMap<String, Integer>();
		for (String topic : topics) {
			topicMap.put(topic, 1);
		}

		HashMap<String, String> kafkaParams = new HashMap<String, String>();
		kafkaParams.put("zookeeper.connect", zkQuorum);
		kafkaParams.put("group.id", consumerGroup);
		kafkaParams.put("zookeeper.connection.timeout.ms", "10000");
		kafkaParams.put("auto.offset.reset", "smallest");

		// parallel reading from Kafka
		List<JavaPairDStream<String, String>> kafkaStreams = new ArrayList<JavaPairDStream<String, String>>(numThreads);
		for (int i = 0; i < numThreads; i++) {
			JavaPairReceiverInputDStream<String, String> messages = KafkaUtils.createStream(jssc, String.class,
					String.class, StringDecoder.class, StringDecoder.class, kafkaParams, topicMap,
					StorageLevel.MEMORY_AND_DISK_SER());
			kafkaStreams.add(messages);
		}
		JavaPairDStream<String, String> unifiedStream = jssc.union(kafkaStreams.get(0),
				kafkaStreams.subList(1, kafkaStreams.size()));

		JavaDStream<String> lines = Utils.getLines(unifiedStream, flightsProcessed);

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// outputs [carrier, 1] or [carrier, 0]
		JavaPairDStream<String, Integer> carrierDelayStream = lines
				.mapToPair(new PairFunction<String, String, Integer>() {

					private static final long serialVersionUID = -7986497144086431718L;

					@Override
					public Tuple2<String, Integer> call(String x) {
						String[] values = COMMA.split(x);

						String carrier = values[4];
						int arrivalDelay = Integer.valueOf(values[10]);

						if (arrivalDelay == 0) {
							return new Tuple2<String, Integer>(carrier, 1);
						} else {
							return new Tuple2<String, Integer>(carrier, 0);
						}
					}
				});

		Function2<List<Integer>, Optional<PerformanceStatsCalculator>, Optional<PerformanceStatsCalculator>> updateFunction = new Function2<List<Integer>, Optional<PerformanceStatsCalculator>, Optional<PerformanceStatsCalculator>>() {

			private static final long serialVersionUID = 2967315431762739039L;

			@Override
			public Optional<PerformanceStatsCalculator> call(List<Integer> values,
					Optional<PerformanceStatsCalculator> state) {
				int newOntimeCount = 0;
				for (Integer value : values) {
					newOntimeCount += value;
				}

				PerformanceStatsCalculator ontimeAndTotal = state.isPresent() ? state.get()
						: new PerformanceStatsCalculator();
				ontimeAndTotal.stat += newOntimeCount;
				ontimeAndTotal.total += values.size();

				return Optional.of(ontimeAndTotal);
			}
		};

		JavaPairDStream<String, PerformanceStatsCalculator> runningCounts = carrierDelayStream
				.updateStateByKey(updateFunction);

		final List<ColumnDefinition> columns = new ArrayList<>();
		columns.add(new ColumnDefinition("carrier", "text"));
		columns.add(new ColumnDefinition("arr_performance", "float"));
		final String[] primaryKeyColumns = new String[] { "carrier" };

		runningCounts.foreachRDD(new VoidFunction<JavaPairRDD<String, PerformanceStatsCalculator>>() {

			private static final long serialVersionUID = 7234764440325615097L;

			@Override
			public void call(JavaPairRDD<String, PerformanceStatsCalculator> rdd) {
				String keyspace = "t2g12";
				String table = keyspace + "." + keyspace;
				CassandraDriver cassandra = new CassandraDriver(cassandraIP, keyspace, columns, primaryKeyColumns);
				Session cassandraSession = cassandra.getSession();

				for (Tuple2<String, PerformanceStatsCalculator> t : rdd.collect()) {
					String carrier = t._1();
					float performance = t._2().get();

					cassandra.getSession().execute("INSERT INTO " + table + " (carrier, arr_performance) VALUES ('"
							+ carrier + "', " + performance + ")");
				}

				ResultSet resultSet = cassandraSession.execute("SELECT carrier, arr_performance FROM " + table);
				List<FlightPerformanceForComparison> carriers = new ArrayList<>();
				while (!resultSet.isExhausted()) {
					Row row = resultSet.one();
					FlightPerformanceForComparison flightPerformance = new FlightPerformanceForComparison();
					flightPerformance.criteria1 = row.getString(0);
					flightPerformance.performance = row.getFloat(1);
					carriers.add(flightPerformance);
				}
				Collections.sort(carriers);

				int i = 0;
				for (Iterator<FlightPerformanceForComparison> iterator = carriers.iterator(); iterator.hasNext();) {
					FlightPerformanceForComparison carrierPerformance = iterator.next();

					if (i > 9) { // delete the ones that aren't in the top 10
						cassandraSession.execute(
								"DELETE FROM " + table + " WHERE carrier = '" + carrierPerformance.criteria1 + "'");
					}

					i++;
				}

				cassandra.close();

				Utils.printProgress(flightsProcessed, start);
			}
		});

		// Start the computation
		jssc.start();
		jssc.awaitTermination();
	}

}
