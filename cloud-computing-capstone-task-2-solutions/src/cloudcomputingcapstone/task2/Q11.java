package cloudcomputingcapstone.task2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaPairReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.google.common.base.Optional;

import cloudcomputingcapstone.Utils;
import cloudcomputingcapstone.cassandra.CassandraDriver;
import cloudcomputingcapstone.cassandra.ColumnDefinition;
import kafka.serializer.StringDecoder;
import scala.Tuple2;

public final class Q11 {

	private static final Pattern COMMA = Pattern.compile(",");

	public static void main(String[] args) {
		if (args.length < 5) {
			System.err.println("Usage: <zkQuorum> <group> <topics> <numThreads> <cassandra>");
			System.exit(1);
		}

		String zkQuorum = args[0];
		String consumerGroup = args[1];
		String[] topics = args[2].split(",");
		int numThreads = Integer.parseInt(args[3]);
		final String cassandraIP = args[4];

		SparkConf sparkConf = new SparkConf().setAppName(Q11.class.getName());
		sparkConf.set("spark.streaming.unpersist", "false");
		sparkConf.set("spark.streaming.receiver.writeAheadLog.enable", "true");

		JavaStreamingContext jssc = new JavaStreamingContext(sparkConf, Durations.seconds(5));
		jssc.checkpoint("/tmp");

		final Accumulator<Integer> flightsProcessed = jssc.sparkContext().intAccumulator(0);
		final Accumulator<Double> start = jssc.sparkContext().accumulator(System.currentTimeMillis());

		Map<String, Integer> topicMap = new HashMap<String, Integer>();
		for (String topic : topics) {
			topicMap.put(topic, 1);
		}

		HashMap<String, String> kafkaParams = new HashMap<String, String>();
		kafkaParams.put("zookeeper.connect", zkQuorum);
		kafkaParams.put("group.id", consumerGroup);
		kafkaParams.put("zookeeper.connection.timeout.ms", "10000");
		kafkaParams.put("auto.offset.reset", "smallest");

		// parallel reading from Kafka
		List<JavaPairDStream<String, String>> kafkaStreams = new ArrayList<JavaPairDStream<String, String>>(numThreads);
		for (int i = 0; i < numThreads; i++) {
			JavaPairReceiverInputDStream<String, String> messages = KafkaUtils.createStream(jssc, String.class,
					String.class, StringDecoder.class, StringDecoder.class, kafkaParams, topicMap,
					StorageLevel.MEMORY_AND_DISK_SER());
			kafkaStreams.add(messages);
		}
		JavaPairDStream<String, String> unifiedStream = jssc.union(kafkaStreams.get(0),
				kafkaStreams.subList(1, kafkaStreams.size()));

		// get the lines
		JavaDStream<String> lines = unifiedStream.map(new Function<Tuple2<String, String>, String>() {

			private static final long serialVersionUID = -2167626588552066519L;

			@Override
			public String call(Tuple2<String, String> tuple2) {
				flightsProcessed.add(1);
				return tuple2._2();
			}
		});

		// outputs [origin, 1] and [destination, 1]
		JavaDStream<Tuple2<String, Integer>> originsDestinations = lines
				.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>() {

					private static final long serialVersionUID = -7986497144086431718L;

					// 2008,3,8,4106,MQ,GRR,ORD,1425,1,66,1

					@Override
					public Iterable<Tuple2<String, Integer>> call(String x) {
						String[] split = COMMA.split(x);

						String origin = split[5];
						Tuple2<String, Integer> originTuple = new Tuple2<String, Integer>(origin, 1);

						String destination = split[6];
						Tuple2<String, Integer> destinationTuple = new Tuple2<String, Integer>(destination, 1);

						List<Tuple2<String, Integer>> outTuples = new ArrayList<>();
						outTuples.add(originTuple);
						outTuples.add(destinationTuple);
						return outTuples;
					}
				});

		// map/reduce
		JavaPairDStream<String, Integer> popularityCount = originsDestinations
				.mapToPair(new PairFunction<Tuple2<String, Integer>, String, Integer>() {

					private static final long serialVersionUID = -7258499830444516975L;

					@Override
					public Tuple2<String, Integer> call(Tuple2<String, Integer> t) {
						return new Tuple2<String, Integer>(t._1, t._2);
					}
				});

		Function2<List<Integer>, Optional<Integer>, Optional<Integer>> updateFunction = new Function2<List<Integer>, Optional<Integer>, Optional<Integer>>() {

			private static final long serialVersionUID = 2967315431762739039L;

			@Override
			public Optional<Integer> call(List<Integer> values, Optional<Integer> state) {
				Integer count = state.or(0);
				for (Integer newCount : values) {
					count += newCount;
				}
				return Optional.of(count);
			}
		};

		JavaPairDStream<String, Integer> runningCounts = popularityCount.updateStateByKey(updateFunction);

		final List<ColumnDefinition> columns = new ArrayList<>();
		columns.add(new ColumnDefinition("airport", "text"));
		columns.add(new ColumnDefinition("popularity", "int"));
		final String[] primaryKeyColumns = new String[] { "airport" };

		runningCounts.foreachRDD(new VoidFunction<JavaPairRDD<String, Integer>>() {

			private static final long serialVersionUID = 7234764440325615097L;

			@Override
			public void call(JavaPairRDD<String, Integer> rdd) {
				String keyspace = "t2g11";
				String table = keyspace + "." + keyspace;
				CassandraDriver cassandra = new CassandraDriver(cassandraIP, keyspace, columns, primaryKeyColumns);
				Session cassandraSession = cassandra.getSession();

				for (Tuple2<String, Integer> t : rdd.collect()) {
					String airport = t._1();
					cassandra.getSession().execute("INSERT INTO " + table + " (airport, popularity) VALUES ('" + airport
							+ "', " + t._2() + ")"); // "upsert"
				}

				ResultSet resultSet = cassandraSession.execute("SELECT airport, popularity FROM " + table);
				List<FlightPerformanceForComparison> airports = new ArrayList<>();
				while (!resultSet.isExhausted()) {
					Row row = resultSet.one();
					FlightPerformanceForComparison airportPerformance = new FlightPerformanceForComparison();
					airportPerformance.criteria1 = row.getString(0);
					airportPerformance.performance = row.getInt(1);
					airports.add(airportPerformance);
				}
				Collections.sort(airports);

				int i = 0;
				for (Iterator<FlightPerformanceForComparison> iterator = airports.iterator(); iterator.hasNext();) {
					FlightPerformanceForComparison airportPerformance = iterator.next();

					if (i > 9) { // delete the ones that aren't in the top 10
						cassandraSession.execute(
								"DELETE FROM " + table + " WHERE airport = '" + airportPerformance.criteria1 + "'");
					}

					i++;
				}

				cassandra.close();

				Utils.printProgress(flightsProcessed, start);
			}
		});

		// Start the computation
		jssc.start();
		jssc.awaitTermination();
	}

}
