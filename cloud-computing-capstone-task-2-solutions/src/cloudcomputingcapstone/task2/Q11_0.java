package cloudcomputingcapstone.task2;
// package cloudcomputingcapstone.task2.q11;
//
// import java.util.ArrayList;
// import java.util.Arrays;
// import java.util.HashMap;
// import java.util.HashSet;
// import java.util.List;
// import java.util.regex.Pattern;
//
// import org.apache.spark.SparkConf;
// import org.apache.spark.api.java.JavaPairRDD;
// import org.apache.spark.api.java.function.FlatMapFunction;
// import org.apache.spark.api.java.function.Function;
// import org.apache.spark.api.java.function.Function2;
// import org.apache.spark.api.java.function.PairFunction;
// import org.apache.spark.api.java.function.VoidFunction;
// import org.apache.spark.streaming.Durations;
// import org.apache.spark.streaming.api.java.JavaDStream;
// import org.apache.spark.streaming.api.java.JavaPairDStream;
// import org.apache.spark.streaming.api.java.JavaPairInputDStream;
// import org.apache.spark.streaming.api.java.JavaStreamingContext;
// import org.apache.spark.streaming.kafka.KafkaUtils;
//
// import kafka.serializer.StringDecoder;
// import scala.Tuple2;
//
// public final class Q11_0 {
//
// private static final Pattern COMMA = Pattern.compile(",");
//
// public static void main(String[] args) {
// if (args.length < 2) {
// System.err.println("Usage: Class <brokers> <topics> <duration>\n"
// + " <brokers> is a list of one or more Kafka brokers\n"
// + " <topics> is a list of one or more kafka topics to consume from\n"
// + " <duration> is the duration of the batch in seconds\n"
// + " example: broker1-host:port,broker2-host:port topic1,topic2 60\n");
// System.exit(1);
// }
//
// String brokers = args[0];
// String topics = args[1];
// long duration = Long.valueOf(args[2]);
// System.out.println(brokers);
// System.out.println(topics);
//
// // Create context with a 2 seconds batch interval
// SparkConf sparkConf = new SparkConf().setAppName("Q11");
// JavaStreamingContext jssc = new JavaStreamingContext(sparkConf, Durations.seconds(duration));
//
// HashSet<String> topicsSet = new HashSet<String>(Arrays.asList(topics.split(",")));
// HashMap<String, String> kafkaParams = new HashMap<String, String>();
// kafkaParams.put("metadata.broker.list", brokers);
// kafkaParams.put("auto.offset.reset", "smallest");
//
// // Create direct kafka stream with brokers and topics
// JavaPairInputDStream<String, String> messages = KafkaUtils.createDirectStream(jssc, String.class, String.class,
// StringDecoder.class, StringDecoder.class, kafkaParams, topicsSet);
//
// // get the lines
// JavaDStream<String> lines = messages.map(new Function<Tuple2<String, String>, String>() {
//
// private static final long serialVersionUID = -2167626588552066519L;
//
// @Override
// public String call(Tuple2<String, String> tuple2) {
// return tuple2._2();
// }
// });
//
// // outputs [origin, 1] and [destination, 1]
// JavaDStream<Tuple2<String, Integer>> originsDestinations = lines
// .flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>() {
//
// private static final long serialVersionUID = -7986497144086431718L;
//
// // 2008,3,8,4106,MQ,GRR,ORD,1425,1,66,1
//
// @Override
// public Iterable<Tuple2<String, Integer>> call(String x) {
// String[] split = COMMA.split(x);
//
// String origin = split[5];
// Tuple2<String, Integer> originTuple = new Tuple2<String, Integer>(origin, 1);
//
// String destination = split[6];
// Tuple2<String, Integer> destinationTuple = new Tuple2<String, Integer>(destination, 1);
//
// List<Tuple2<String, Integer>> outTuples = new ArrayList<>();
// outTuples.add(originTuple);
// outTuples.add(destinationTuple);
// return outTuples;
// }
// });
//
// // map/reduce
// JavaPairDStream<String, Integer> popularityCount = originsDestinations
// .mapToPair(new PairFunction<Tuple2<String, Integer>, String, Integer>() {
//
// private static final long serialVersionUID = -7258499830444516975L;
//
// @Override
// public Tuple2<String, Integer> call(Tuple2<String, Integer> t) {
// return new Tuple2<String, Integer>(t._1, t._2);
// }
// }).reduceByKey(new Function2<Integer, Integer, Integer>() {
//
// private static final long serialVersionUID = -7665935532595134955L;
//
// @Override
// public Integer call(Integer i1, Integer i2) {
// return i1 + i2;
// }
// });
//
// // top 10
//
// JavaPairDStream<Integer, String> swappedCounts = popularityCount
// .mapToPair(new PairFunction<Tuple2<String, Integer>, Integer, String>() {
//
// private static final long serialVersionUID = -1213837032675069766L;
//
// @Override
// public Tuple2<Integer, String> call(Tuple2<String, Integer> in) {
// return in.swap();
// }
// });
//
// JavaPairDStream<Integer, String> sortedCounts = swappedCounts
// .transformToPair(new Function<JavaPairRDD<Integer, String>, JavaPairRDD<Integer, String>>() {
//
// private static final long serialVersionUID = 439286251404416927L;
//
// @Override
// public JavaPairRDD<Integer, String> call(JavaPairRDD<Integer, String> in) throws Exception {
// return in.sortByKey(false);
// }
// });
//
// sortedCounts.foreachRDD(new VoidFunction<JavaPairRDD<Integer, String>>() {
//
// private static final long serialVersionUID = 7234764440325615097L;
//
// @Override
// public void call(JavaPairRDD<Integer, String> rdd) {
// String out = "\nTop 10 airports:\n";
// for (Tuple2<Integer, String> t : rdd.take(10)) {
// out = out + t._2() + "\t" + t._1() + "\n";
// }
// System.out.println(out);
// }
//
// });
//
// // wordCounts.print();
//
// // Start the computation
// jssc.start();
// jssc.awaitTermination();
// }
//
// }
