package cloudcomputingcapstone.task2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaPairReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;

import com.datastax.driver.core.Session;
import com.google.common.base.Optional;

import cloudcomputingcapstone.Utils;
import cloudcomputingcapstone.cassandra.CassandraDriver;
import cloudcomputingcapstone.cassandra.ColumnDefinition;
import kafka.serializer.StringDecoder;
import scala.Tuple2;

public final class Q24 {

	private static final Pattern COMMA = Pattern.compile(",");

	public static void main(String[] args) {
		if (args.length < 5) {
			System.err.println("Usage: <zkQuorum> <group> <topics> <numThreads> <cassandra>");
			System.exit(1);
		}

		String zkQuorum = args[0];
		String consumerGroup = args[1];
		String[] topics = args[2].split(",");
		int numThreads = Integer.parseInt(args[3]);
		final String cassandraIP = args[4];

		SparkConf sparkConf = new SparkConf().setAppName(Q24.class.getName());
		sparkConf.set("spark.streaming.unpersist", "false");
		sparkConf.set("spark.streaming.receiver.writeAheadLog.enable", "true");

		JavaStreamingContext jssc = new JavaStreamingContext(sparkConf, Durations.seconds(5));
		jssc.checkpoint("/tmp");

		final Accumulator<Integer> flightsProcessed = jssc.sparkContext().intAccumulator(0);
		final Accumulator<Double> start = jssc.sparkContext().accumulator(System.currentTimeMillis());

		Map<String, Integer> topicMap = new HashMap<String, Integer>();
		for (String topic : topics) {
			topicMap.put(topic, 1);
		}

		HashMap<String, String> kafkaParams = new HashMap<String, String>();
		kafkaParams.put("zookeeper.connect", zkQuorum);
		kafkaParams.put("group.id", consumerGroup);
		kafkaParams.put("zookeeper.connection.timeout.ms", "10000");
		kafkaParams.put("auto.offset.reset", "smallest");

		// parallel reading from Kafka
		List<JavaPairDStream<String, String>> kafkaStreams = new ArrayList<JavaPairDStream<String, String>>(numThreads);
		for (int i = 0; i < numThreads; i++) {
			JavaPairReceiverInputDStream<String, String> messages = KafkaUtils.createStream(jssc, String.class,
					String.class, StringDecoder.class, StringDecoder.class, kafkaParams, topicMap,
					StorageLevel.MEMORY_AND_DISK_SER());
			kafkaStreams.add(messages);
		}
		JavaPairDStream<String, String> unifiedStream = jssc.union(kafkaStreams.get(0),
				kafkaStreams.subList(1, kafkaStreams.size()));

		JavaDStream<String> lines = Utils.getLines(unifiedStream, flightsProcessed);

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		JavaPairDStream<String, Integer> originDestinationDelayStream = lines
				.mapToPair(new PairFunction<String, String, Integer>() {

					private static final long serialVersionUID = -7986497144086431718L;

					@Override
					public Tuple2<String, Integer> call(String x) {
						String[] values = COMMA.split(x);

						String origin = values[5];
						String destination = values[6];
						int arrivalDelay = Integer.valueOf(values[9]);

						return new Tuple2<String, Integer>(origin + "," + destination, arrivalDelay);
					}
				});

		Function2<List<Integer>, Optional<PerformanceStatsCalculator>, Optional<PerformanceStatsCalculator>> updateFunction = new Function2<List<Integer>, Optional<PerformanceStatsCalculator>, Optional<PerformanceStatsCalculator>>() {

			private static final long serialVersionUID = 2967315431762739039L;

			@Override
			public Optional<PerformanceStatsCalculator> call(List<Integer> values, Optional<PerformanceStatsCalculator> state) {
				int totalArrivalDelay = 0;
				for (Integer value : values) {
					totalArrivalDelay += value;
				}

				PerformanceStatsCalculator performanceStats = state.isPresent() ? state.get() : new PerformanceStatsCalculator();
				performanceStats.stat += totalArrivalDelay;
				performanceStats.total += values.size();

				return Optional.of(performanceStats);
			}
		};

		JavaPairDStream<String, PerformanceStatsCalculator> runningCounts = originDestinationDelayStream
				.updateStateByKey(updateFunction);

		final List<ColumnDefinition> columns = new ArrayList<>();
		columns.add(new ColumnDefinition("origin", "text"));
		columns.add(new ColumnDefinition("destination", "text"));
		columns.add(new ColumnDefinition("mean_arrival_delay", "float"));
		final String[] primaryKeyColumns = new String[] { "origin", "destination" };

		runningCounts.foreachRDD(new VoidFunction<JavaPairRDD<String, PerformanceStatsCalculator>>() {

			private static final long serialVersionUID = 7234764440325615097L;

			@Override
			public void call(JavaPairRDD<String, PerformanceStatsCalculator> rdd) {
				String keyspace = "t2g24";
				String table = keyspace + "." + keyspace;
				CassandraDriver cassandra = new CassandraDriver(cassandraIP, keyspace, columns, primaryKeyColumns);
				Session cassandraSession = cassandra.getSession();

				////////////////////

				for (Tuple2<String, PerformanceStatsCalculator> t : rdd.collect()) {
					String[] originDestination = t._1().split(",");
					String origin = originDestination[0];
					String destination = originDestination[1];

					PerformanceStatsCalculator performanceStats = t._2();

					// "upsert" only
					cassandraSession
							.execute("INSERT INTO " + table + " (origin, destination, mean_arrival_delay) VALUES ('"
									+ origin + "', '" + destination + "', " + performanceStats.get() + ")");
				}

				cassandra.close();

				Utils.printProgress(flightsProcessed, start);
			}
		});

		// Start the computation
		jssc.start();
		jssc.awaitTermination();
	}

}
