package cloudcomputingcapstone.task2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaPairReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;

import com.datastax.driver.core.Session;
import com.google.common.base.Optional;

import cloudcomputingcapstone.Constants;
import cloudcomputingcapstone.Utils;
import cloudcomputingcapstone.cassandra.CassandraDriver;
import cloudcomputingcapstone.cassandra.ColumnDefinition;
import kafka.serializer.StringDecoder;
import scala.Tuple2;

public final class Q32_UpdateState {

	public static void main(String[] args) {
		if (args.length < 5) {
			System.err.println("Usage: <zkQuorum> <group> <topics> <numThreads> <cassandra>");
			System.exit(1);
		}

		String zkQuorum = args[0];
		String consumerGroup = args[1];
		String[] topics = args[2].split(",");
		int numThreads = Integer.parseInt(args[3]);
		final String cassandraIP = args[4];

		SparkConf sparkConf = new SparkConf().setAppName(Q32_UpdateState.class.getName());
		sparkConf.set("spark.streaming.unpersist", "false");
		sparkConf.set("spark.streaming.receiver.writeAheadLog.enable", "true");

		JavaStreamingContext jssc = new JavaStreamingContext(sparkConf, Durations.seconds(3));
		jssc.checkpoint("/tmp");

		final Accumulator<Integer> flightsProcessed = jssc.sparkContext().intAccumulator(0);
		final Accumulator<Double> start = jssc.sparkContext().accumulator(System.currentTimeMillis());
		final Accumulator<Double> dbInserting = jssc.sparkContext().accumulator(0f);

		Map<String, Integer> topicMap = new HashMap<String, Integer>();
		for (String topic : topics) {
			topicMap.put(topic, 1);
		}

		HashMap<String, String> kafkaParams = new HashMap<String, String>();
		kafkaParams.put("zookeeper.connect", zkQuorum);
		kafkaParams.put("group.id", consumerGroup);
		kafkaParams.put("zookeeper.connection.timeout.ms", "10000");
		kafkaParams.put("auto.offset.reset", "smallest");

		// parallel reading from Kafka
		List<JavaPairDStream<String, String>> kafkaStreams = new ArrayList<JavaPairDStream<String, String>>(numThreads);
		for (int i = 0; i < numThreads; i++) {
			JavaPairReceiverInputDStream<String, String> messages = KafkaUtils.createStream(jssc, String.class,
					String.class, StringDecoder.class, StringDecoder.class, kafkaParams, topicMap,
					StorageLevel.MEMORY_AND_DISK_SER());
			kafkaStreams.add(messages);
		}
		JavaPairDStream<String, String> unifiedStream = jssc.union(kafkaStreams.get(0),
				kafkaStreams.subList(1, kafkaStreams.size()));

		JavaDStream<String> lines = Utils.getLines(unifiedStream, flightsProcessed);

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		JavaPairDStream<String, FlightCSVFormat> originDestinationDelayStream = lines
				.mapToPair(new PairFunction<String, String, FlightCSVFormat>() {

					private static final long serialVersionUID = -7986497144086431718L;

					@Override
					public Tuple2<String, FlightCSVFormat> call(String x) {
						FlightCSVFormat flightCSVFormat = FlightCSVFormat.parseCSVLine(x);

						return new Tuple2<String, FlightCSVFormat>(
								flightCSVFormat.getFlightKeyForMininumDelayMapReduce(), flightCSVFormat);
					}
				});

		Function2<List<FlightCSVFormat>, Optional<FlightCSVFormat>, Optional<FlightCSVFormat>> updateFunction = new Function2<List<FlightCSVFormat>, Optional<FlightCSVFormat>, Optional<FlightCSVFormat>>() {

			private static final long serialVersionUID = 2967315431762739039L;

			@Override
			public Optional<FlightCSVFormat> call(List<FlightCSVFormat> values, Optional<FlightCSVFormat> state) {
				// update only if the delay is strictly lower, if not, just maintain the old one.
				FlightCSVFormat newFlightCSVFormat = state.isPresent() ? state.get() : null;
				for (FlightCSVFormat value : values) {
					if (newFlightCSVFormat == null || value.getArrivalDelay() < newFlightCSVFormat.getArrivalDelay()) {
						newFlightCSVFormat = value;
					}
				}

				return Optional.of(newFlightCSVFormat);
			}
		};

		JavaPairDStream<String, FlightCSVFormat> minimumDelays = originDestinationDelayStream
				.updateStateByKey(updateFunction);

		JavaPairDStream<String, FlightCSVFormat> firstLegFlights = minimumDelays
				.filter(new Function<Tuple2<String, FlightCSVFormat>, Boolean>() {

					private static final long serialVersionUID = 5561288034025651175L;

					@Override
					public Boolean call(Tuple2<String, FlightCSVFormat> v1) throws Exception {
						if (v1._1().endsWith("1")) {
							return true;
						}
						return false;
					}

				});

		JavaPairDStream<String, FlightCSVFormat> secondLegFlights = minimumDelays
				.filter(new Function<Tuple2<String, FlightCSVFormat>, Boolean>() {

					private static final long serialVersionUID = 5561288034025651175L;

					@Override
					public Boolean call(Tuple2<String, FlightCSVFormat> v1) throws Exception {
						if (v1._1().endsWith("2")) {
							return true;
						}
						return false;
					}

				});

		// JavaPairDStream<String, String> flightCombinations = firstLegFlights.transformWithToPair(
		// secondLegFlights,
		// new Function3<JavaPairRDD<String, FlightCSVFormat>, JavaPairRDD<String, FlightCSVFormat>, Time,
		// JavaPairRDD<String, String>>() {
		//
		// @Override
		// public JavaPairRDD<String, String> call(JavaPairRDD<String, FlightCSVFormat> rdd1,
		// JavaPairRDD<String, FlightCSVFormat> rdd2, Time time) throws Exception {
		// JavaPairRDD<Tuple2<String, FlightCSVFormat>, Tuple2<String, FlightCSVFormat>> cartesian = rdd1
		// .cartesian(rdd2);
		//
		// JavaPairRDD<String,String> mapToPair = cartesian
		// .filter(new Function<Tuple2<Tuple2<String, FlightCSVFormat>, Tuple2<String, FlightCSVFormat>>, Boolean>() {
		//
		// @Override
		// public Boolean call(
		// Tuple2<Tuple2<String, FlightCSVFormat>, Tuple2<String, FlightCSVFormat>> twoFlights)
		// throws Exception {
		//
		// return null;
		// }
		//
		// }).mapToPair(new PairFunction<Tuple2<Tuple2<String,FlightCSVFormat>,Tuple2<String,FlightCSVFormat>>, String,
		// String>() {
		//
		// @Override
		// public Tuple2<String, String> call(
		// Tuple2<Tuple2<String, FlightCSVFormat>, Tuple2<String, FlightCSVFormat>> t)
		// throws Exception {
		// // TODO Auto-generated method stub
		// return null;
		// }
		//
		// });
		//
		// return mapToPair;
		// }
		// });

		final List<ColumnDefinition> columns = new ArrayList<>();
		columns.add(new ColumnDefinition("x", "text"));
		columns.add(new ColumnDefinition("y", "text"));
		columns.add(new ColumnDefinition("day", "int"));
		columns.add(new ColumnDefinition("month", "int"));
		columns.add(new ColumnDefinition("year", "int"));
		columns.add(new ColumnDefinition("flight_xy", "text"));
		columns.add(new ColumnDefinition("dep_time", "text"));
		columns.add(new ColumnDefinition("delay", "int"));
		final String[] primaryKeyColumns = new String[] { "x", "y", "day", "month", "year" };

		firstLegFlights.foreachRDD(new VoidFunction<JavaPairRDD<String, FlightCSVFormat>>() {

			private static final long serialVersionUID = 7234764440325615097L;

			@Override
			public void call(JavaPairRDD<String, FlightCSVFormat> rdd) {
				String keyspace = "t2g32";
				String table = keyspace + "." + keyspace;
				String tableSuffix = "leg1";
				String fullTableName = table + tableSuffix;
				CassandraDriver cassandra = new CassandraDriver(cassandraIP, keyspace, tableSuffix, columns,
						primaryKeyColumns);
				Session cassandraSession = cassandra.getSession();

				////////////////////

				for (Tuple2<String, FlightCSVFormat> t : rdd.collect()) {
					FlightCSVFormat flightCSVFormat = t._2();
					String origin = flightCSVFormat.getOrigin();
					String destination = flightCSVFormat.getDestination();
					int day = Integer.valueOf(flightCSVFormat.getDay());
					int month = Integer.valueOf(flightCSVFormat.getMonth());
					int year = Integer.valueOf(flightCSVFormat.getYear());
					String flight = flightCSVFormat.getFlightNum();
					String departure = flightCSVFormat.getDepartureTime();
					int delay = flightCSVFormat.getArrivalDelay();

					// "upsert" only
					cassandraSession.execute("INSERT INTO " + fullTableName
							+ " (x, y, day, month, year, flight_xy, dep_time, delay) VALUES ('" + origin + "', '"
							+ destination + "', " + day + ", " + month + ", " + year + ", '" + flight + "', '"
							+ departure + "', " + delay + ")");
				}

				cassandra.close();

				Utils.printProgress(Constants.TOTAL_FLIGHTS_32, flightsProcessed, start);
			}
		});

		secondLegFlights.foreachRDD(new VoidFunction<JavaPairRDD<String, FlightCSVFormat>>() {

			private static final long serialVersionUID = 7234764440325615097L;

			@Override
			public void call(JavaPairRDD<String, FlightCSVFormat> rdd) {
				String keyspace = "t2g32";
				String table = keyspace + "." + keyspace;
				String tableSuffix = "leg2";
				String fullTableName = table + tableSuffix;
				CassandraDriver cassandra = new CassandraDriver(cassandraIP, keyspace, tableSuffix, columns,
						primaryKeyColumns);
				Session cassandraSession = cassandra.getSession();

				////////////////////

				for (Tuple2<String, FlightCSVFormat> t : rdd.collect()) {
					FlightCSVFormat flightCSVFormat = t._2();
					String origin = flightCSVFormat.getOrigin();
					String destination = flightCSVFormat.getDestination();
					int day = Integer.valueOf(flightCSVFormat.getDay());
					int month = Integer.valueOf(flightCSVFormat.getMonth());
					int year = Integer.valueOf(flightCSVFormat.getYear());
					String flight = flightCSVFormat.getFlightNum();
					String departure = flightCSVFormat.getDepartureTime();
					int delay = flightCSVFormat.getArrivalDelay();

					// "upsert" only
					long startInserting = System.currentTimeMillis();
					cassandraSession.execute("INSERT INTO " + fullTableName
							+ " (x, y, day, month, year, flight_xy, dep_time, delay) VALUES ('" + origin + "', '"
							+ destination + "', " + day + ", " + month + ", " + year + ", '" + flight + "', '"
							+ departure + "', " + delay + ")");
					dbInserting.add(Double.valueOf(System.currentTimeMillis() - startInserting));
				}

				cassandra.close();

				Utils.printProgress(Constants.TOTAL_FLIGHTS_32, flightsProcessed, start);
				System.out.println("insert db: " + (dbInserting.value() / 1000) + "s");
			}
		});

		// Start the computation
		jssc.start();
		jssc.awaitTermination();
	}

}
