package cloudcomputingcapstone;

import java.text.DecimalFormat;

import org.apache.spark.Accumulator;
import org.apache.spark.AccumulatorParam;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;

import scala.Tuple2;

public class Utils {

	public static JavaDStream<String> getLines(JavaPairDStream<String, String> stream,
			final Accumulator<Integer> flightsProcessed) {
		JavaDStream<String> lines = stream.map(new Function<Tuple2<String, String>, String>() {

			private static final long serialVersionUID = -2167626588552066519L;

			@Override
			public String call(Tuple2<String, String> tuple2) {
				flightsProcessed.add(1);
				return tuple2._2();
			}

		});

		return lines;
	}

	private static final DecimalFormat TWO_PLACES = new DecimalFormat("#.##");

	public static void printProgress(int totalFlights, Accumulator<Integer> flightsProcessed,
			Accumulator<Double> start) {
		Integer flightsSoFar = flightsProcessed.value();

		double percentage = ((double) flightsSoFar) / totalFlights * 100;

		int remainingFlights = totalFlights - flightsSoFar;

		double elapsedTime = System.currentTimeMillis() - start.value();

		double speed = ((double) flightsSoFar) / elapsedTime * 1000; // flights per second

		double remainingTime = remainingFlights / speed / 60; // in minutes

		StringBuilder progress = new StringBuilder();
		progress.append("flights processed so far:\t");
		progress.append(flightsProcessed);
		progress.append("\t/ ");
		progress.append(TWO_PLACES.format(percentage));
		progress.append("%\t/ ");
		progress.append(remainingFlights);
		progress.append(" flights to go\t/ elapsed ");
		progress.append(TWO_PLACES.format(elapsedTime / 1000 / 60));
		progress.append(" min\t/ ");
		progress.append(TWO_PLACES.format(speed));
		progress.append(" flights per sec\t/ ETA ");
		progress.append(TWO_PLACES.format(remainingTime));
		progress.append(" min");

		System.out.println(progress.toString());

		if (flightsSoFar >= totalFlights) {
			System.out.println("FINISHED PROCESSING!!! YOU CAN KILL THE APP!!!");
		}
	}

	public static void printProgress(Accumulator<Integer> flightsProcessed, Accumulator<Double> start) {
		printProgress(Constants.TOTAL_FLIGHTS, flightsProcessed, start);
	}

	public static void main(String[] args) {
		Accumulator<Integer> flightsProcessed = new Accumulator<Integer>(13000000, new AccumulatorParam<Integer>() {

			private static final long serialVersionUID = 5232481268975572796L;

			@Override
			public Integer addInPlace(Integer arg0, Integer arg1) {
				return null;
			}

			@Override
			public Integer zero(Integer arg0) {
				return null;
			}

			@Override
			public Integer addAccumulator(Integer arg0, Integer arg1) {
				return null;
			}

		});

		Accumulator<Double> start = new Accumulator<Double>(((double) System.currentTimeMillis() - (30 * 60 * 1000)),
				new AccumulatorParam<Double>() {

					private static final long serialVersionUID = 7617136426944289675L;

					@Override
					public Double addInPlace(Double arg0, Double arg1) {
						return null;
					}

					@Override
					public Double zero(Double arg0) {
						return null;
					}

					@Override
					public Double addAccumulator(Double arg0, Double arg1) {
						return null;
					}

				});

		printProgress(flightsProcessed, start);
	}

}
