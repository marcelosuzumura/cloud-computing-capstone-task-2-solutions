package cloudcomputingcapstone.cassandra;

import java.io.Serializable;

public class ColumnDefinition implements Serializable {

	private static final long serialVersionUID = -8389263306235619865L;

	public String name;
	public String type;

	public ColumnDefinition(String name, String type) {
		this.name = name;
		this.type = type;
	}

}
