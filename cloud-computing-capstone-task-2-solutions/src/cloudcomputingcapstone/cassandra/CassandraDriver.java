package cloudcomputingcapstone.cassandra;

import java.util.List;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

public class CassandraDriver {

	private Cluster cluster;
	private Session session;

	public CassandraDriver() {
		//
	}

	public CassandraDriver(String ip, String table, List<ColumnDefinition> columns, String[] primaryKeyColumns) {
		this(ip, table, "", columns, primaryKeyColumns);
	}

	public CassandraDriver(String ip, String table, String tableSuffix, List<ColumnDefinition> columns,
			String[] primaryKeyColumns) {
		this.connect(ip);
		this.createKeyspace(table);
		this.createTable(table, tableSuffix, columns, primaryKeyColumns);
	}

	public Session getSession() {
		return this.session;
	}

	public void connect(String node) {
		this.cluster = Cluster.builder().addContactPoint(node).build();
		this.session = this.cluster.connect();
	}

	public void createKeyspace(String keyspace) {
		this.session.execute("CREATE KEYSPACE IF NOT EXISTS " + keyspace
				+ " WITH replication = {'class' : 'SimpleStrategy', 'replication_factor' : 2};");
	}

	public void createTable(String tableName, String tableNameSuffix, List<ColumnDefinition> columns,
			String[] primaryKeyColumns) {
		StringBuilder tableSB = new StringBuilder();
		tableSB.append("CREATE TABLE IF NOT EXISTS ");
		tableSB.append(tableName);
		tableSB.append(".");
		tableSB.append(tableName);
		tableSB.append(tableNameSuffix);
		tableSB.append("(");
		for (ColumnDefinition column : columns) {
			tableSB.append(column.name);
			tableSB.append(" ");
			tableSB.append(column.type);
			tableSB.append(",");
		}
		tableSB.append("PRIMARY KEY (");
		for (String column : primaryKeyColumns) {
			tableSB.append(column);
			tableSB.append(",");
		}
		tableSB.deleteCharAt(tableSB.length() - 1);
		tableSB.append("));");

		this.session.execute(tableSB.toString());
	}

	public void createTable(String tableName, List<ColumnDefinition> columns, String[] primaryKeyColumns) {
		this.createTable(tableName, "", columns, primaryKeyColumns);
	}

	public void close() {
		this.session.close();
		this.cluster.close();
	}

}
